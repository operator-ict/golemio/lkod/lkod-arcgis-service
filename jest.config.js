module.exports = {
    setupFiles: ["./test/setup.ts"],
    moduleFileExtensions: ["ts", "js", "json"],
    moduleNameMapper: {
        // Application paths
        "^#application$": "<rootDir>/src/application",
        "^#application/(.*)$": "<rootDir>/src/application/$1",

        // Domain paths
        "^#domain$": "<rootDir>/src/domain",
        "^#domain/(.*)$": "<rootDir>/src/domain/$1",

        // Infrastructure paths
        "^#infrastructure$": "<rootDir>/src/infrastructure",
        "^#infrastructure/(.*)$": "<rootDir>/src/infrastructure/$1",

        // Framework paths
        "^#framework$": "<rootDir>/src/framework",
        "^#framework/(.*)$": "<rootDir>/src/framework/$1",
    },
    transform: {
        "^.+\\.ts$": ["ts-jest"],
    },
    testRegex: "./test/.*.test.ts",
    testEnvironment: "node",
    collectCoverageFrom: ["./src/**/*.ts"],
    coverageReporters: ["text", "cobertura"],
};
