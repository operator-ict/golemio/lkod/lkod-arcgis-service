# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.1.3] - 2024-12-19

### Fixed

-   error when iana url is undefined (TXT, GPKG, GDB... formats): TypeError: Cannot read properties of undefined (reading 'replace')
-   error parsing geoElements: TypeError: iterable is not iterable

## [1.1.2] - 2024-10-29

### Security

-   Update dependencies to fix security vulnerabilities

## [1.1.1] - 2024-08-19

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.1.0] - 2024-08-13

### Changed

-   Update Node.js to 20.16.0 ([lkod#126](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/126))

## [1.0.0] - 2023-12-13

### Added

-   Fetch ArcGIS datasets from the DCAT-AP 2.0.1 feed

