import { ILogLevels } from "@golemio/core/dist/helpers/logger";

export interface IConfiguration {
    nodeEnv: "development" | "test" | "production";
    logLevel: ILogLevels;
    postgresConn: string | null;
    postgresSchema: string;
    postgresIdleTimeout: string;
    postgresMinConnections: string;
    postgresMaxConnections: string;
    defaultTimezone: string;
    refreshDataCron: string | null;
    lkodCatalogUrl: string | null;
}
