import { ILogLevels } from "@golemio/core/dist/helpers/logger";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { IConfiguration } from "./interfaces/IConfiguration";

@injectable()
export class Configuration implements IConfiguration {
    public nodeEnv = (process.env.NODE_ENV as IConfiguration["nodeEnv"]) ?? "development";
    public logLevel = (process.env.LOG_LEVEL as ILogLevels) ?? "info";
    public postgresConn = process.env.POSTGRES_CONN ?? null;
    public postgresSchema = process.env.POSTGRES_SCHEMA ?? "public";
    public postgresIdleTimeout = process.env.POSTGRES_IDLE_TIMEOUT ?? "";
    public postgresMinConnections = process.env.POSTGRES_MIN_CONNECTIONS ?? "";
    public postgresMaxConnections = process.env.POSTGRES_MAX_CONNECTIONS ?? "";
    public defaultTimezone = process.env.DEFAULT_TIMEZONE ?? "Europe/Prague";
    public refreshDataCron = process.env.REFRESH_DATA_CRON ?? null;
    public lkodCatalogUrl = process.env.LKOD_CATALOG_URL ?? null;
}
