import { createLogger } from "@golemio/core/dist/helpers/logger";
import { container as frameworkContainer, instanceCachingFactory } from "@golemio/core/dist/shared/tsyringe";
import { Configuration } from "../Configuration";
import { IConfiguration } from "../interfaces/IConfiguration";
import { FrameworkToken } from "./FrameworkToken";

// Registrations
const config = frameworkContainer
    .registerSingleton(FrameworkToken.Configuration, Configuration)
    .resolve<IConfiguration>(FrameworkToken.Configuration);

frameworkContainer.register(FrameworkToken.Logger, {
    useFactory: instanceCachingFactory(() =>
        createLogger({
            projectName: "lkod-arcgis-service",
            nodeEnv: config.nodeEnv,
            logLevel: config.logLevel,
        })
    ),
});

export { frameworkContainer };
