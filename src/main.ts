/* istanbul ignore file */
import "@golemio/core/dist/shared/_global";

import { ILkodArcgisServiceApp } from "#application/interfaces/ILkodArcgisServiceApp";
import { ApplicationToken } from "#application/ioc/ApplicationToken";
import { applicationContainer } from "#application/ioc/Di";
import { FrameworkToken } from "#framework/ioc/FrameworkToken";
import { ILogger } from "@golemio/core/dist/helpers/logger";

// Resolve application instance and start the app
const app = applicationContainer.resolve<ILkodArcgisServiceApp>(ApplicationToken.LkodArcgisServiceApp);
app.start();

// Handle process signals
process.on("SIGTERM", () => applicationContainer.dispose());
process.on("SIGINT", () => applicationContainer.dispose());

for (const event of ["uncaughtException", "unhandledRejection"]) {
    const log = applicationContainer.resolve<ILogger>(FrameworkToken.Logger);

    process.on(event as NodeJS.Signals, async (err) => {
        // Log error, gracefully shut down and exit with exit code 2
        log.error(err);
        await applicationContainer.dispose();
        process.exit(2);
    });
}
