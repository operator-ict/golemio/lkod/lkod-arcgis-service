//import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { FatalError } from "@golemio/core/dist/shared/golemio-errors";
import fs from "fs";
import path from "path";
import { Parser } from "xml2js";
import { IMapper } from "./IMapper";

//@injectable()
export class FileTypeMapper implements IMapper {
    protected mappingTable: Record<string, { mediaType: string; iana: string; code: string }> | undefined;
    protected euUrl = "http://publications.europa.eu/resource/authority/file-type/";
    protected fileName = "filetypes.xml";
    protected rootElement = "filetypes";
    private regex = /[^\/]+\/[^\/]+$/;

    public async load() {
        const pathToFile = path.join(__dirname, "../../../lookups/" + this.fileName);

        const themeXmlContent = fs.readFileSync(pathToFile, "utf8");
        const parser = new Parser();
        const themeJson = await parser.parseStringPromise(themeXmlContent);

        const mapping = themeJson[this.rootElement].record.map((record: any) => {
            return {
                mediaType: record["internet-media-type"][0]["_"],
                iana: record["conformsTo"] ? record["conformsTo"][0] : undefined,
                code: record["authority-code"][0],
            };
        });

        this.mappingTable = mapping
            .filter((element: any) => element.iana)
            .reduce((acc: Record<string, string>, curr: any) => {
                acc[this.regex.exec(curr.iana)![0]] = curr;
                return acc;
            }, {});

        this.mappingTable = mapping.reduce((acc: Record<string, string>, curr: any) => {
            acc[curr.code] = curr;
            return acc;
        }, this.mappingTable);
    }

    public get(identifier: string): { euUrl: string; iana: string } {
        if (this.mappingTable === undefined) {
            throw new FatalError("Mapping table is not loaded", this.constructor.name);
        }
        identifier = identifier.replace("ftype/", "");

        const mappedItem = this.mappingTable[identifier] ?? this.mappingTable[identifier.toUpperCase()];

        return { euUrl: this.euUrl + mappedItem.code, iana: mappedItem.iana?.replace("https://", "http://") };
    }
}
