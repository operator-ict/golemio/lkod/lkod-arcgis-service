import { FatalError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import fs from "fs";
import path from "path";
import { Parser } from "xml2js";
import { IMapper } from "./IMapper";

export abstract class AbstractEuMapper implements IMapper {
    protected mappingTable: Record<string, string> | undefined;
    protected abstract euUrl: string;
    protected abstract fileName: string;
    protected abstract rootElement: string;

    public async load() {
        const pathToFile = path.join(__dirname, "../../../lookups/" + this.fileName);

        const themeXmlContent = fs.readFileSync(pathToFile, "utf8");
        const parser = new Parser();
        const themeJson = await parser.parseStringPromise(themeXmlContent);

        const mapping = themeJson[this.rootElement].record
            .map((record: any) => {
                const list = record.label[0]["lg.version"].filter((lgversion: any) => lgversion.$.lg === "ces");
                return list.map((element: any) => {
                    return {
                        cs: element["_"],
                        code: record["authority-code"][0],
                    };
                });
            })
            .flat();

        this.mappingTable = mapping.reduce((acc: Record<string, string>, curr: any) => {
            acc[curr.cs] = curr.code;
            return acc;
        }, {});
    }

    public get(csKey: string): string {
        if (this.mappingTable === undefined) {
            throw new FatalError("Mapping table is not loaded", this.constructor.name);
        }

        if (this.mappingTable[csKey] === undefined) {
            throw new GeneralError(`Key "${csKey}" not found in mapping table: ${this.fileName}`, this.constructor.name);
        }

        return this.euUrl + this.mappingTable[csKey];
    }
}
