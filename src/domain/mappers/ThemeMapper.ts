import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractEuMapper } from "./AbstractEuMapper";
import { IMapper } from "./IMapper";

@injectable()
export class ThemeMapper extends AbstractEuMapper implements IMapper {
    protected euUrl = "http://publications.europa.eu/resource/authority/data-theme/";
    protected fileName = "data-theme.xml";
    protected rootElement = "data-theme";
}
