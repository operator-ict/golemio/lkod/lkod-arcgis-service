import { FatalError } from "@golemio/core/dist/shared/golemio-errors";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { IMapper } from "./IMapper";

@injectable()
export class IprFileTypeMapper implements IMapper {
    protected mappingTable: Record<string, { mime: string; format: string }> | undefined;
    private euUrl = "http://publications.europa.eu/resource/authority/file-type/";
    private ianaUrl = "http://www.iana.org/assignments/media-types/";

    public load() {
        this.mappingTable = {
            dwg: { mime: "application/acad", format: "OCTET" }, //TODO iana
            dgn: { mime: "application/acad", format: "OCTET" }, //TODO iana
            dxf: { mime: "application/dxf", format: "OCTET" }, //TODO iana
            shp: { mime: "application/zip", format: "SHP" },
            shapefile: { mime: "application/zip", format: "SHP" },
            gml: { mime: "text/sgml", format: "GML" }, //TODO iana
            csv: { mime: "text/csv", format: "CSV" },
            geojson: { mime: "application/json", format: "GEOJSON" },
            tiff: { mime: "image/tiff", format: "TIFF" },
        };
    }

    public get(code: string | undefined) {
        code = code?.split(" ")[0].toLowerCase();
        if (this.mappingTable === undefined) {
            throw new FatalError("Mapping table is not loaded", this.constructor.name);
        }

        const mapping = this.mappingTable[code ?? ""];

        return mapping
            ? {
                  format: this.euUrl + mapping.format,
                  mediaType: this.ianaUrl + mapping.mime,
              }
            : {};
    }
}
