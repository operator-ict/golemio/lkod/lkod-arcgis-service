export interface IMapper {
    load(): void | Promise<void>;
    get(code: any): any;
}
