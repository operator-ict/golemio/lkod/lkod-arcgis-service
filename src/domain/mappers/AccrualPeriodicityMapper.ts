import { FatalError } from "@golemio/core/dist/shared/golemio-errors";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { IMapper } from "./IMapper";

@injectable()
export class AccrualPeriodicityMapper implements IMapper {
    protected mappingTable: Record<string, { ipr: string; eu: string }> | undefined;
    protected euUrl = "http://publications.europa.eu/resource/authority/frequency/";

    public load() {
        this.mappingTable = {
            "000": { ipr: "Empty/Nevyplněno", eu: "UNKNOWN" },
            "001": { ipr: "Continual/Průběžně", eu: "UPDATE_CONT" },
            "002": { ipr: "Daily/Denně", eu: "DAILY" },
            "003": { ipr: "Weekly/Týdenně", eu: "WEEKLY" },
            "004": { ipr: "Fortnightly/Dvoutýdenně", eu: "BIWEEKLY" },
            "005": { ipr: "Monthly/Měsíčně", eu: "MONTHLY" },
            "006": { ipr: "Quarterly/Čtvrtletně", eu: "QUARTERLY" },
            "007": { ipr: "Biannually/Dvouleté", eu: "BIENNIAL" },
            "008": { ipr: "Annually/Roční", eu: "ANNUAL" },
            "009": { ipr: "As needed/Dle potřeby", eu: "OTHER" },
            "010": { ipr: "Irregular/Nepravidelně", eu: "IRREG" },
            "011": { ipr: "Not planned/Neplánované", eu: "NEVER" },
            "012": { ipr: "Unknown/Neznámé", eu: "UNKNOWN" },
        };
    }

    public get(csKey: string): string {
        if (this.mappingTable === undefined) {
            throw new FatalError("Mapping table is not loaded", this.constructor.name);
        }

        return this.euUrl + this.mappingTable[csKey].eu;
    }
}
