import { FatalError } from "@golemio/core/dist/shared/golemio-errors";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { IMapper } from "./IMapper";

interface IDataExt {
    geoEle: Array<
        Partial<{
            GeoDesc: {
                geoId: {
                    identCode: string;
                };
            };
        }>
    >;
}

@injectable()
export class IprRuianMapper implements IMapper {
    protected mappingTable: Record<string, string> | undefined;

    public load() {
        this.mappingTable = {
            "1": "https://linked.cuzk.cz/resource/ruian/stat/1",
            "19": "https://linked.cuzk.cz/resource/ruian/region-soudrznosti/19",
        };
    }

    public get(dataExt: IDataExt | IDataExt[]): string[] {
        if (this.mappingTable === undefined) {
            throw new FatalError("Mapping table is not loaded", this.constructor.name);
        }

        const result: string[] = [];
        const geoElement = dataExt instanceof Array ? dataExt[0].geoEle : dataExt.geoEle;
        const iterable = geoElement instanceof Array ? geoElement : [geoElement];

        for (const element of iterable) {
            if (element?.GeoDesc?.geoId?.identCode) {
                result.push(this.mappingTable[element.GeoDesc.geoId.identCode]);
            }
        }

        return result;
    }
}
