import { FatalError } from "@golemio/core/dist/shared/golemio-errors";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { IMapper } from "./IMapper";

@injectable()
export class IprLicenceMapper implements IMapper {
    protected mappingTable: Record<string, string> | undefined;

    public load() {
        this.mappingTable = {
            CC0: "https://creativecommons.org/publicdomain/zero/1.0/",
            "CC BY": "https://creativecommons.org/licenses/by/4.0/",
            "CC BY-SA": "https://creativecommons.org/licenses/by-sa/4.0/",
        };
    }

    public get(code: string): string {
        if (this.mappingTable === undefined) {
            throw new FatalError("Mapping table is not loaded", this.constructor.name);
        }

        return this.mappingTable[code];
    }
}
