import { DomainToken } from "#domain/ioc/DomainToken";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IMapper } from "./IMapper";

@injectable()
export class MapperLoader {
    constructor(
        @inject(DomainToken.ThemeMapper) private themeMapper: IMapper,
        @inject(DomainToken.AccrualPeriodicityMapper) private accrualPeriodicityMapper: IMapper,
        @inject(DomainToken.IprRuianMapper) private iprRuianMapper: IMapper,
        @inject(DomainToken.IprFileTypeMapper) private iprFileTypeMapper: IMapper,
        @inject(DomainToken.IprLicenceMapper) private iprLicenceMapper: IMapper,
        @inject(DomainToken.FileTypeMapper) private fileTypeMapper: IMapper
    ) {}

    public async initializeMappers(): Promise<void> {
        await this.themeMapper.load();
        await this.accrualPeriodicityMapper.load();
        await this.iprRuianMapper.load();
        await this.iprFileTypeMapper.load();
        await this.iprLicenceMapper.load();
        await this.fileTypeMapper.load();
    }
}
