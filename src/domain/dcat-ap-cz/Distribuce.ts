import { Nazev } from "./helpers/Nazev";
import { PristupovaSluzba } from "./helpers/PristupovaSluzba";
import { SpecifikacePodminekUziti } from "./helpers/SpecifikacePodminekUziti";

export interface Distribuce {
    iri: string;
    typ: "Distribuce";
    podmínky_užití: SpecifikacePodminekUziti;
    přístupové_url: string;
    soubor_ke_stažení?: string;
    formát?: string;
    typ_média?: string;
    schéma?: string;
    typ_média_komprese?: string;
    typ_média_balíčku?: string;
    název?: Nazev;
    přístupová_služba?: PristupovaSluzba;
}
