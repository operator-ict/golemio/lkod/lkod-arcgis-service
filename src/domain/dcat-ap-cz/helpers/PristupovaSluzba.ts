import { Nazev } from "./Nazev";

export interface PristupovaSluzba {
    iri: string;
    název: Nazev;
    popis_přístupového_bodu?: string;
    přístupový_bod?: string;
    specifikace?: string[] | string;
    typ: "Datová služba";
}
