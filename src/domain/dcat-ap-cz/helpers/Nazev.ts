/**
 * Vícejazyčný text
 */
export interface Nazev {
    /**
     * Text česky
     */
    cs?: string;
    /**
     * Text anglicky
     */
    en?: string;
}
