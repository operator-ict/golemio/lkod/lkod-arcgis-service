/**
 * Více vícejazyčných textů
 */

export interface KlicoveSlovo {
    cs?: string[] | string;
    en?: string[] | string;
}
