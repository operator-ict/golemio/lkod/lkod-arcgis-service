import { Nazev } from "./Nazev";

export interface SpecifikacePodminekUziti {
    autor?: Nazev;
    autor_databáze?: Nazev;
    autorské_dílo: string;
    databáze_chráněná_zvláštními_právy: string;
    databáze_jako_autorské_dílo: string;
    osobní_údaje: any;
    typ: "Specifikace podmínek užití";
}
