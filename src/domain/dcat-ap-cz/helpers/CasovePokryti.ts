export interface CasovePokryti {
    konec?: Date;
    typ: "Časový interval";
    začátek?: Date;
}
