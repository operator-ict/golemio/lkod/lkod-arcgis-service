import { Nazev } from "./Nazev";

export interface KontaktniBod {
    "e-mail"?: string;
    jméno?: Nazev;
    typ: string;
}
