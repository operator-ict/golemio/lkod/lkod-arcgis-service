import { CasovePokryti } from "./helpers/CasovePokryti";
import { Distribuce } from "./Distribuce";
import { KlicoveSlovo } from "./helpers/KlicoveSlovo";
import { KontaktniBod } from "./helpers/KontaktniBod";
import { Nazev } from "./helpers/Nazev";
/*
 * based on version https://ofn.gov.cz/rozhran%C3%AD-katalog%C5%AF-otev%C5%99en%C3%BDch-dat/2021-01-11/sch%C3%A9mata/datov%C3%A1-sada.json
 */

export interface DatovaSada {
    "@context": "https://ofn.gov.cz/rozhraní-katalogů-otevřených-dat/2021-01-11/kontexty/rozhraní-katalogů-otevřených-dat.jsonld";
    iri: string;
    typ: "Datová sada";
    název: Nazev;
    popis: Nazev;
    poskytovatel: string;
    téma: string[];
    periodicita_aktualizace: string;
    klíčové_slovo: KlicoveSlovo;
    geografické_území?: string[];
    prvek_rúian: string[];
    časové_pokrytí?: CasovePokryti;
    kontaktní_bod?: KontaktniBod;
    dokumentace?: string;
    specifikace?: string[];
    koncept_euroVoc?: string[];
    prostorové_rozlišení_v_metrech?: number | string;
    časové_rozlišení?: string;
    je_součástí?: string;
    distribuce?: Distribuce[];
}
