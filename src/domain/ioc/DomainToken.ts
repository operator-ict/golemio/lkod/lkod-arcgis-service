export const DomainToken = {
    HttpFeedService: Symbol(),
    CronTask: Symbol(),
    ThemeMapper: Symbol(),
    AccrualPeriodicityMapper: Symbol(),
    IprRuianMapper: Symbol(),
    IprFileTypeMapper: Symbol(),
    IprLicenceMapper: Symbol(),
    IprTransformation: Symbol(),
    MapperLoader: Symbol(),
    FileTypeMapper: Symbol(),
    DatasetFacade: Symbol(),
};
