import { AccrualPeriodicityMapper } from "#domain/mappers/AccrualPeriodicityMapper";
import { FileTypeMapper } from "#domain/mappers/FileTypeMapper";
import { IprFileTypeMapper } from "#domain/mappers/IprFileTypeMapper";
import { IprLicenceMapper } from "#domain/mappers/IprLicenceMapper";
import { IprRuianMapper } from "#domain/mappers/IprRuianMapper";
import { MapperLoader } from "#domain/mappers/MapperLoader";
import { ThemeMapper } from "#domain/mappers/ThemeMapper";
import { IprTransformation } from "#domain/transformations/IprTransformation";
import { infrastructureContainer } from "#infrastructure/ioc/Di";
import { DatasetFacade } from "#domain/service/DatasetFacade";
import { HttpFeedService } from "../HttpFeedService";
import { RefreshArcgisDefinitionsTask } from "../tasks/RefreshArcgisDefinitionsTask";
import { DomainToken } from "./DomainToken";

const domainContainer = infrastructureContainer.createChildContainer();

// Registrations

domainContainer.registerSingleton(DomainToken.HttpFeedService, HttpFeedService);
domainContainer.registerSingleton(DomainToken.ThemeMapper, ThemeMapper);
domainContainer.registerSingleton(DomainToken.AccrualPeriodicityMapper, AccrualPeriodicityMapper);
domainContainer.registerSingleton(DomainToken.IprRuianMapper, IprRuianMapper);
domainContainer.registerSingleton(DomainToken.IprFileTypeMapper, IprFileTypeMapper);
domainContainer.registerSingleton(DomainToken.IprLicenceMapper, IprLicenceMapper);
domainContainer.registerSingleton(DomainToken.IprTransformation, IprTransformation);
domainContainer.registerSingleton(DomainToken.MapperLoader, MapperLoader);
domainContainer.registerSingleton(DomainToken.FileTypeMapper, FileTypeMapper);
domainContainer.registerSingleton(DomainToken.DatasetFacade, DatasetFacade);
domainContainer.registerSingleton(DomainToken.CronTask, RefreshArcgisDefinitionsTask);

export { domainContainer };
