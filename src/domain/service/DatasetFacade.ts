import { DomainToken } from "#domain/ioc/DomainToken";
import { IprTransformation } from "#domain/transformations/IprTransformation";
import { IConfiguration } from "#framework/interfaces/IConfiguration";
import { FrameworkToken } from "#framework/ioc/FrameworkToken";
import { InfrastructureToken } from "#infrastructure/ioc/InfrastructureToken";
import { IArcgisFeedOrganizationDao } from "#infrastructure/models/interfaces/IArcgisFeedOrganizationDao";
import IDatasetInfo from "#infrastructure/repositories/interfaces/IDatasetInfo";
import { IDatasetRepository } from "#infrastructure/repositories/interfaces/IDatasetRepository";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { v4 } from "uuid";
import { IDatasetFacade } from "./interfaces/IDatasetFacade";

@injectable()
export class DatasetFacade implements IDatasetFacade {
    constructor(
        @inject(InfrastructureToken.DatasetRepository) private datasetRepository: IDatasetRepository,
        @inject(DomainToken.IprTransformation) private iprTransformation: IprTransformation,
        @inject(FrameworkToken.Configuration) private config: IConfiguration,
        @inject(FrameworkToken.Logger) private logger: ILogger
    ) {}

    public async saveDatasets(datasets: any[], organization: IArcgisFeedOrganizationDao): Promise<void> {
        const result: IDatasetInfo[] = [];
        const errResults: Array<{ err: string; dataset: any }> = [];

        for (const dataset of datasets) {
            try {
                const arcgisId = dataset["dct:identifier"];
                const datasetId = await this.datasetRepository.getDatasetId(arcgisId);
                const id = datasetId ?? v4();
                const transformedDataset = this.iprTransformation.transform(dataset, id, organization);
                result.push({ id, arcgisId, dataset: transformedDataset, isUpdate: !!datasetId });
            } catch (err) {
                errResults.push({ err: err.toString(), dataset });
                this.logger.error(
                    // eslint-disable-next-line max-len
                    `${this.constructor.name}: ${organization.name}: error while processing dataset ${dataset["dct:identifier"]}: ${err}`
                );
            }
        }

        if (errResults.length > 0) {
            this.logger.warn(
                // eslint-disable-next-line max-len
                `Unable to process some datasets count: ${errResults.length}, refresh of organization ${organization.slug} will proceed with valid datasets.`
            );
        }

        await this.datasetRepository.refreshOrganizationDatasets(organization.id, result);
    }
}
