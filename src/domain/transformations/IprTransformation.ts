import { DatovaSada } from "#domain/dcat-ap-cz/DatovaSada";
import { Distribuce } from "#domain/dcat-ap-cz/Distribuce";
import { DomainToken } from "#domain/ioc/DomainToken";
import { AccrualPeriodicityMapper } from "#domain/mappers/AccrualPeriodicityMapper";
import { FileTypeMapper } from "#domain/mappers/FileTypeMapper";
import { IprFileTypeMapper } from "#domain/mappers/IprFileTypeMapper";
import { IprLicenceMapper } from "#domain/mappers/IprLicenceMapper";
import { IprRuianMapper } from "#domain/mappers/IprRuianMapper";
import { ThemeMapper } from "#domain/mappers/ThemeMapper";
import { IConfiguration } from "#framework/interfaces/IConfiguration";
import { FrameworkToken } from "#framework/ioc/FrameworkToken";
import { IArcgisFeedOrganizationDao } from "#infrastructure/models/interfaces/IArcgisFeedOrganizationDao";
import { HTMLUtils } from "@golemio/core/dist/helpers/transformation/HTMLUtils";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { JSONPath } from "jsonpath-plus";
import path from "path";
import slug from "slugify";
import { ITransformation } from "./interfaces/ITransformation";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";

@injectable()
export class IprTransformation implements ITransformation {
    private datasetsRelativePath = "/lod/catalog/";
    private organizationRelativePath = "/organization/";

    constructor(
        @inject(DomainToken.ThemeMapper) private themeMapper: ThemeMapper,
        @inject(DomainToken.AccrualPeriodicityMapper) private accrualPeriodicityMapper: AccrualPeriodicityMapper,
        @inject(DomainToken.IprRuianMapper) private iprRuianMapper: IprRuianMapper,
        @inject(DomainToken.IprFileTypeMapper) private iprFileTypeMapper: IprFileTypeMapper,
        @inject(DomainToken.IprLicenceMapper) private iprLicenceMapper: IprLicenceMapper,
        @inject(DomainToken.FileTypeMapper) private fileTypeMapper: FileTypeMapper,
        @inject(FrameworkToken.Configuration) private config: IConfiguration,
        @inject(FrameworkToken.Logger) private logger: ILogger
    ) {}

    public transform(dataset: any, id: string, organization: IArcgisFeedOrganizationDao) {
        const newDistributions: Distribuce[] = [];

        const newDataset: DatovaSada = {
            "@context":
                "https://ofn.gov.cz/rozhraní-katalogů-otevřených-dat/2021-01-11/kontexty/rozhraní-katalogů-otevřených-dat.jsonld",
            iri: new URL(this.datasetsRelativePath + id, this.config.lkodCatalogUrl!).toString(),
            typ: "Datová sada",
            název: { cs: dataset["dct:title"] },
            popis: { cs: HTMLUtils.outputStructuredText(dataset["dct:description"]) ?? "" },
            poskytovatel: new URL(
                path.join(this.organizationRelativePath, organization.slug),
                this.config.lkodCatalogUrl!
            ).toString(),
            téma: [
                this.themeMapper.get(
                    JSONPath<string>({ path: "dct:metadata.dataIdInfo.idCitation.otherCitDet", json: dataset, wrap: false })
                ),
            ],
            periodicita_aktualizace: this.accrualPeriodicityMapper.get(
                JSONPath<string>({
                    path: "dct:metadata.dataIdInfo.resMaint.maintFreq.MaintFreqCd.@_value",
                    json: dataset,
                    wrap: false,
                })
            ),
            klíčové_slovo: { cs: dataset["dcat:keyword"] instanceof Array ? dataset["dcat:keyword"] : [dataset["dcat:keyword"]] },
            geografické_území: [
                "https://publications.europa.eu/resource/authority/place/CZE_PRG",
                "https://sws.geonames.org/3067695/",
            ],
            prvek_rúian: this.iprRuianMapper.get(
                JSONPath<any>({ path: "dct:metadata.dataIdInfo.dataExt", json: dataset, wrap: false })
            ),
            kontaktní_bod: {
                typ: "Organizace",
                jméno: {
                    cs: JSONPath<string>({ path: "dct:metadata.mdContact.rpOrgName", json: dataset, wrap: false }),
                },
                "e-mail": JSONPath<string>({ path: "dcat:contactPoint.vcard:hasEmail", json: dataset, wrap: false }),
            },
            dokumentace: dataset["@id"],
            specifikace: dataset["dct:conformsTo"],
            distribuce: newDistributions,
        };

        for (let index = 0; index < dataset["dcat:distribution"].length; index++) {
            const distTransformed = this.transformDistributions(dataset["dcat:distribution"][index], dataset, newDataset.iri);
            if (distTransformed) {
                newDistributions.push(distTransformed);
            }
        }

        return newDataset;
    }

    private transformDistributions(distribuce: any, dataset: any, datasetIri: string) {
        const formatMapping = this.fileTypeMapper.get(distribuce["dct:format"]["@id"]);
        if (!formatMapping.iana) {
            this.logger.warn(`Filtering out ${distribuce["dct:title"]} distribution, datasetIri: ${datasetIri}`);
            return;
        }

        const newDistribution: Distribuce = {
            iri: "",
            typ: "Distribuce",
            název: { cs: distribuce["dct:title"] },
            podmínky_užití: {
                autorské_dílo: this.iprLicenceMapper.get(dataset["dct:license"]),
                databáze_chráněná_zvláštními_právy:
                    "https://data.gov.cz/podmínky-užití/není-chráněna-zvláštním-právem-pořizovatele-databáze/",
                databáze_jako_autorské_dílo: "https://data.gov.cz/podmínky-užití/není-autorskoprávně-chráněnou-databází/",
                osobní_údaje: "https://data.gov.cz/podmínky-užití/neobsahuje-osobní-údaje/",
                typ: "Specifikace podmínek užití",
                autor: {
                    cs: JSONPath<string>({ path: "dct:metadata.mdContact.rpOrgName", json: dataset, wrap: false }),
                },
            },
            přístupové_url: distribuce["dcat:accessURL"]["@id"],
            soubor_ke_stažení: distribuce["dcat:accessURL"]["@id"],
            formát: formatMapping.euUrl,
            typ_média: formatMapping.iana,
        };
        newDistribution.iri = datasetIri + "/distributions/" + this.resolveDistributionIriSufix(newDistribution);

        return newDistribution;
    }

    private resolveDistributionIriSufix(distribution: any): string {
        let type = "none";
        let urlSlug = "none";

        if (distribution.formát) {
            type = distribution.formát.substring(distribution.formát.lastIndexOf("/") + 1);
            type = type.toLowerCase();
        } else {
            type = "url";
        }

        if (distribution.přístupové_url) {
            urlSlug = slug(distribution.přístupové_url, { lower: true, remove: /[*+~()'"!:@]/g }).replace(/\//g, "_");
            urlSlug = urlSlug.slice(-20);
        }

        return `${type}-${urlSlug}`;
    }
}
