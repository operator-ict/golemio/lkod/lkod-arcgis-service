import { IArcgisFeedOrganizationDao } from "#infrastructure/models/interfaces/IArcgisFeedOrganizationDao";

export interface ITransformation {
    transform(dataset: any, id: string, oragnization: IArcgisFeedOrganizationDao): any;
}
