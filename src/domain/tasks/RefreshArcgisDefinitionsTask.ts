import { IHttpFeedService } from "#domain/interfaces/IHttpFeedService";
import { DomainToken } from "#domain/ioc/DomainToken";
import { DatasetFacade } from "#domain/service/DatasetFacade";
import { IConfiguration } from "#framework/interfaces/IConfiguration";
import { FrameworkToken } from "#framework/ioc/FrameworkToken";
import { InfrastructureToken } from "#infrastructure/ioc/InfrastructureToken";
import { IArcgisFeedOrganizationDao } from "#infrastructure/models/interfaces/IArcgisFeedOrganizationDao";
import { IOrganizationRepository } from "#infrastructure/repositories/interfaces/IOrganizationRepository";
import { FatalError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractCronTask } from "./AbstractCronTask";
import { ICronTask } from "./interfaces/ICronTask";

@injectable()
export class RefreshArcgisDefinitionsTask extends AbstractCronTask implements ICronTask {
    constructor(
        @inject(FrameworkToken.Configuration) private config: IConfiguration,
        @inject(InfrastructureToken.OrganizationRepository) private organizationRepository: IOrganizationRepository,
        @inject(DomainToken.DatasetFacade) private datasetFacade: DatasetFacade,
        @inject(DomainToken.HttpFeedService) private httpFeedService: IHttpFeedService
    ) {
        super();
    }

    protected get cronExpression(): string {
        if (!this.config.refreshDataCron) {
            throw new FatalError("No refresh data cron expression provided");
        }

        return this.config.refreshDataCron;
    }

    protected async execute(): Promise<void> {
        this.logger.info(`${this.constructor.name}: executing`);

        const organizations = await this.organizationRepository.findAll();
        if (organizations.length === 0) {
            this.logger.warn(`${this.constructor.name}: no organizations found`);
            return;
        }

        this.logger.info(`${this.constructor.name}: got ${organizations.length} organizations`);

        for (const organization of organizations) {
            await this.fetchDatasetsForOrganization(organization);
        }
    }

    private async fetchDatasetsForOrganization(organization: IArcgisFeedOrganizationDao): Promise<void> {
        const datasets = await this.httpFeedService.getDcatDatasets(organization.arcGisFeed);

        if (datasets.length === 0) {
            this.logger.warn(
                `${this.constructor.name}: ${organization.name}: no datasets received from feed ${organization.arcGisFeed}`
            );
            return;
        }

        this.logger.info(`${this.constructor.name}: ${organization.name}: got ${datasets.length} datasets`);
        await this.datasetFacade.saveDatasets(datasets, organization);
    }
}
