import { FrameworkToken } from "#framework/ioc/FrameworkToken";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { FatalError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Cron } from "croner";
import { domainContainer } from "../ioc/Di";
import { ICronTask } from "./interfaces/ICronTask";

export abstract class AbstractCronTask implements ICronTask {
    protected logger: ILogger;
    protected cronJob: Cron | null = null;

    constructor() {
        this.logger = domainContainer.resolve<ILogger>(FrameworkToken.Logger);
    }

    public register(timezone: string): void {
        this.cronJob = Cron(this.cronExpression, this.executeWithLogging, {
            timezone,
        });
    }

    public stop(): void {
        this.cronJob?.stop();
    }

    public isRunning(): boolean {
        return this.cronJob?.isRunning() ?? false;
    }

    protected abstract get cronExpression(): string;
    protected abstract execute(): void | Promise<void>;

    private executeWithLogging = async () => {
        try {
            await this.execute();
        } catch (err) {
            if (err instanceof FatalError) {
                throw err;
            } else if (err instanceof GeneralError) {
                this.logger.warn(err, `${this.constructor.name}: error while executing task`);
            } else {
                this.logger.error(err, `${this.constructor.name}: error while executing task`);
            }
        }
    };
}
