export interface ICronTask {
    register(timezone: string): void | Promise<void>;
    stop(): void;
    isRunning(): boolean;
}
