import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { Dataset, Type as DcatType } from "w3c-dcat";
import { IHttpFeedService } from "./interfaces/IHttpFeedService";
import axios, { AxiosResponse } from "axios";

@injectable()
export class HttpFeedService implements IHttpFeedService {
    public async getDcatDatasets(feedUrl: string): Promise<Dataset[]> {
        let response: AxiosResponse;

        try {
            response = await axios.get(feedUrl, {
                responseType: "json",
            });
        } catch (err) {
            throw new GeneralError("Error while getting DCAT datasets", this.constructor.name, err);
        }

        let datasets: Dataset[] = [];
        if (response.data["@type"] !== DcatType.Catalog || !response.data["dcat:dataset"]) {
            return datasets;
        }

        for (const dataset of response.data["dcat:dataset"]) {
            if (dataset["@type"] !== DcatType.Dataset) {
                continue;
            }

            try {
                datasets.push(
                    new Dataset({
                        "@context": response.data["@context"],
                        ...dataset,
                    })
                );
            } catch (err) {
                throw new GeneralError("Error while parsing DCAT dataset", this.constructor.name, err);
            }
        }

        return datasets;
    }
}
