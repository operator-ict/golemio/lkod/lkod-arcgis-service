import { Dataset } from "w3c-dcat";

export interface IHttpFeedService {
    getDcatDatasets(feedUrl: string): Promise<Dataset[]>;
}
