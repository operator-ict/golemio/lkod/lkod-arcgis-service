import { DatovaSada } from "#domain/dcat-ap-cz/DatovaSada";
import { infrastructureContainer } from "#infrastructure/ioc/Di";
import { InfrastructureToken } from "#infrastructure/ioc/InfrastructureToken";
import { DatasetModel, DatasetStatus } from "#infrastructure/models/DatasetModel";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Op, Transaction } from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import IDatasetInfo from "./interfaces/IDatasetInfo";
import { IDatasetRepository } from "./interfaces/IDatasetRepository";

@injectable()
export class DatasetRepository implements IDatasetRepository {
    public async refreshOrganizationDatasets(organizationId: string, datasets: IDatasetInfo[]): Promise<void> {
        const connection = infrastructureContainer
            .resolve<IPostgresConnector>(InfrastructureToken.PostgresConnector)
            .getConnection();
        const transactionStartDate = new Date();
        const transaction = await connection.transaction();
        try {
            for (const dataset of datasets) {
                if (dataset.isUpdate) {
                    await this.update(dataset, transaction);
                } else {
                    await this.insert(dataset, organizationId, transaction);
                }

                await this.markForDeletion(organizationId, transactionStartDate, transaction);
            }
            await transaction.commit();
        } catch (err) {
            await transaction.rollback();
            throw new GeneralError(
                `Error while updating datasets for organization: ${organizationId}`,
                this.constructor.name,
                err
            );
        }
    }

    public async getDatasetId(arcgisId: string): Promise<string | undefined> {
        const dataset = await DatasetModel.findOne({
            where: {
                arcgisid: arcgisId,
            },
        });

        return dataset?.id;
    }

    /**
     * Mark all datasets for chosen organization for deletion if they are not upgraded in current transaction.
     */
    private async markForDeletion(organizationId: string, transactionStartDate: Date, transaction: Transaction) {
        await DatasetModel.update(
            {
                status: DatasetStatus.deleted,
            },
            {
                where: {
                    organizationId: organizationId,
                    createdAt: {
                        [Op.lte]: transactionStartDate,
                    },
                    [Op.or]: [
                        {
                            updatedAt: {
                                [Op.lte]: transactionStartDate,
                            },
                        },
                        { updatedAt: null },
                    ],
                },
                transaction: transaction,
            }
        );
    }

    private async insert(
        dataset: { id: string; arcgisId: string; dataset: DatovaSada; isUpdate: boolean },
        organizationId: string,
        transaction: Transaction
    ) {
        await DatasetModel.create(
            {
                id: dataset.id,
                organizationId: organizationId,
                status: DatasetStatus.saved,
                userId: 1,
                createdAt: new Date(),
                data: dataset.dataset as any,
                arcgisid: dataset.arcgisId,
            },
            { transaction: transaction }
        );
    }

    private async update(
        dataset: { id: string; arcgisId: string; dataset: DatovaSada; isUpdate: boolean },
        transaction: Transaction
    ) {
        await DatasetModel.update(
            {
                status: DatasetStatus.saved,
                updatedAt: new Date(),
                data: dataset.dataset as any,
            },
            {
                where: {
                    id: dataset.id,
                },
                transaction: transaction,
            }
        );
    }
}
