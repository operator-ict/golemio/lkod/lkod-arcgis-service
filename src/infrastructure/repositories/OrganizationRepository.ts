import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { ArcgisFeedOrganizationModel } from "../models/ArcgisFeedOrganizationModel";
import { IOrganizationRepository } from "./interfaces/IOrganizationRepository";

@injectable()
export class OrganizationRepository implements IOrganizationRepository {
    public async findAll(): Promise<ArcgisFeedOrganizationModel[]> {
        try {
            return await ArcgisFeedOrganizationModel.findAll();
        } catch (err) {
            throw new GeneralError("Error while fetching organizations", this.constructor.name, err);
        }
    }
}
