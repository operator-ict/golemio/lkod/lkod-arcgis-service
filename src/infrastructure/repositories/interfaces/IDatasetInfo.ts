import { DatovaSada } from "#domain/dcat-ap-cz/DatovaSada";

export default interface IDatasetInfo {
    id: string;
    arcgisId: string;
    dataset: DatovaSada;
    isUpdate: boolean;
}
