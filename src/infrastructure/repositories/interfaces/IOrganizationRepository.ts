import { ArcgisFeedOrganizationModel } from "#infrastructure/models/ArcgisFeedOrganizationModel";

export interface IOrganizationRepository {
    findAll(): Promise<ArcgisFeedOrganizationModel[]>;
}
