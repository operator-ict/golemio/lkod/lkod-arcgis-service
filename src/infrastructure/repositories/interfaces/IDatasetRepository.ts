import IDatasetInfo from "./IDatasetInfo";

export interface IDatasetRepository {
    refreshOrganizationDatasets(organization: string, datasets: IDatasetInfo[]): Promise<void>;
    getDatasetId(arcgisId: string): Promise<string | undefined>;
}
