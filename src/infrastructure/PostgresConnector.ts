import { IConfiguration } from "#framework/interfaces/IConfiguration";
import { FrameworkToken } from "#framework/ioc/FrameworkToken";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { FatalError } from "@golemio/core/dist/shared/golemio-errors";
import { Sequelize } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import os from "os";
import { IPostgresConnector } from "./interfaces/IPostgresConnector";
import { ArcgisFeedOrganizationModel } from "./models/ArcgisFeedOrganizationModel";
import { DatasetModel, DatasetSchema } from "./models/DatasetModel";

@injectable()
export class PostgresConnector implements IPostgresConnector {
    private connection?: Sequelize;
    private defaultConnectionString: string | null;
    private poolIdleTimeout: string;
    private poolMinConnections: string;
    private poolMaxConnections: string;

    constructor(
        @inject(FrameworkToken.Configuration) private config: IConfiguration,
        @inject(FrameworkToken.Logger) private log: ILogger
    ) {
        this.defaultConnectionString = config.postgresConn;
        this.poolIdleTimeout = config.postgresIdleTimeout;
        this.poolMinConnections = config.postgresMinConnections;
        this.poolMaxConnections = config.postgresMaxConnections;
    }

    public async connect(connectionString = this.defaultConnectionString): Promise<Sequelize> {
        if (this.connection) {
            return this.connection;
        }

        if (!connectionString) {
            throw new FatalError("The ENV variable POSTGRES_CONN cannot be undefined", this.constructor.name);
        }

        try {
            this.connection = new Sequelize(connectionString, {
                dialectOptions: {
                    application_name: `LKOD ArcGIS Service (${os.hostname()})`,
                    schema: this.config.postgresSchema,
                },
                define: {
                    freezeTableName: true,
                    omitNull: false,
                    timestamps: false,
                },
                dialect: "postgres",
                logging: this.log.silly,
                pool: {
                    acquire: 60000,
                    idle: Number.parseInt(this.poolIdleTimeout) || 10000,
                    min: Number.parseInt(this.poolMinConnections) || 0,
                    max: Number.parseInt(this.poolMaxConnections) || 10,
                },
                retry: {
                    match: [
                        /SequelizeConnectionError/,
                        /SequelizeConnectionRefusedError/,
                        /SequelizeHostNotFoundError/,
                        /SequelizeHostNotReachableError/,
                        /SequelizeInvalidConnectionError/,
                        /SequelizeConnectionTimedOutError/,
                        /TimeoutError/,
                    ],
                    max: 8,
                },
            });

            if (!this.connection) {
                throw new Error("Connection is undefined.");
            }

            await this.connection.authenticate();
            this.log.info("Connected to PostgreSQL!");

            this.initModels(this.connection!);
            return this.connection;
        } catch (err) {
            throw new FatalError("Error while connecting to PostgreSQL.", this.constructor.name, err);
        }
    }

    public getConnection(): Sequelize {
        if (!this.connection) {
            throw new FatalError("Sequelize connection does not exist. First call connect() method.", this.constructor.name);
        }
        return this.connection;
    }

    public async isConnected(): Promise<boolean> {
        try {
            await this.connection!.authenticate();
            return true;
        } catch (err) {
            return false;
        }
    }

    public async disconnect(): Promise<void> {
        this.log.info("PostgreSQL disconnect called");
        if (this.connection) {
            await this.connection.close();
        }
    }

    private initModels(connection: Sequelize): void {
        ArcgisFeedOrganizationModel.init(ArcgisFeedOrganizationModel.attributeModel, {
            schema: this.config.postgresSchema,
            sequelize: connection,
            tableName: ArcgisFeedOrganizationModel.tableName,
        });
        DatasetModel.init(DatasetSchema, {
            schema: this.config.postgresSchema,
            sequelize: connection,
            tableName: "dataset",
        });
    }
}
