import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";

export enum DatasetStatus {
    created = "created",
    saved = "saved",
    published = "published",
    unpublished = "unpublished",
    deleted = "deleted",
}

export interface IDatasetDao {
    id: string;
    userId: number;
    organizationId: string;
    status: DatasetStatus;
    createdAt: Date;
    updatedAt?: Date | null;
    data?: IDatasetData | null;
    validationResult?: IValidationResult | null;
    arcgisid: string | null;
}

export interface IDatasetData {
    // required properties
    "@context": string;
    iri: string;
    poskytovatel: string;
    název: {
        cs?: string | null;
        en?: string | null;
    };
    klíčové_slovo: {
        cs?: string[] | null;
        en?: string[] | null;
    };

    // some additional properties
    [key: string]: any;
}

export interface IValidationResult {
    status: "valid" | "invalid" | "hasWarnings";
    messages: string[] | null;
}

export class DatasetModel extends Model<IDatasetDao> implements IDatasetDao {
    declare id: string;
    declare userId: number;
    declare organizationId: string;
    declare status: DatasetStatus;
    declare createdAt: Date;
    declare updatedAt?: Date | null;
    declare data?: IDatasetData | null;
    declare validationResult?: IValidationResult | null;
    declare arcgisid: string | null;
    // virtual properties
    declare readonly isReadOnly: boolean;
}

export const DatasetSchema: ModelAttributes = {
    id: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
    },
    organizationId: {
        type: DataTypes.BIGINT,
        allowNull: false,
    },
    userId: {
        type: DataTypes.BIGINT,
        allowNull: false,
    },
    status: {
        type: DataTypes.ENUM,
        values: Object.values(DatasetStatus),
        allowNull: false,
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true,
    },
    data: {
        type: DataTypes.JSONB,
        allowNull: true,
    },
    validationResult: {
        type: DataTypes.JSON,
        allowNull: true,
    },
    arcgisid: {
        type: DataTypes.STRING,
        allowNull: true,
    },
};
