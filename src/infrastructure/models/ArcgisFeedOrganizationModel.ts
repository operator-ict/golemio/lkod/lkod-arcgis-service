import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IArcgisFeedOrganizationDao } from "./interfaces/IArcgisFeedOrganizationDao";

export class ArcgisFeedOrganizationModel extends Model<ArcgisFeedOrganizationModel> implements IArcgisFeedOrganizationDao {
    public static tableName = "v_organization_with_arcgis_feed";

    declare id: string;
    declare name: string;
    declare arcGisFeed: string;
    declare slug: string;

    public static attributeModel: ModelAttributes<ArcgisFeedOrganizationModel> = {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
        },
        name: {
            type: DataTypes.STRING,
        },
        arcGisFeed: {
            type: DataTypes.STRING,
        },
        slug: {
            type: DataTypes.STRING,
        },
    };
}
