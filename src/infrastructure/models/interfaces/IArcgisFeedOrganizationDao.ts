export interface IArcgisFeedOrganizationDao {
    id: string;
    name: string;
    arcGisFeed: string;
    slug: string;
}
