import { Sequelize } from "@golemio/core/dist/shared/sequelize";

export interface IPostgresConnector {
    connect(connectionString?: string): Promise<Sequelize>;
    getConnection(): Sequelize;
    isConnected(): Promise<boolean>;
    disconnect(): Promise<void>;
}
