import { frameworkContainer } from "#framework/ioc/Di";
import { PostgresConnector } from "#infrastructure/PostgresConnector";
import { DatasetRepository } from "#infrastructure/repositories/DatasetRepository";
import { OrganizationRepository } from "#infrastructure/repositories/OrganizationRepository";
import { InfrastructureToken } from "./InfrastructureToken";

const infrastructureContainer = frameworkContainer.createChildContainer();

// Registrations
infrastructureContainer.registerSingleton(InfrastructureToken.PostgresConnector, PostgresConnector);
infrastructureContainer.registerSingleton(InfrastructureToken.OrganizationRepository, OrganizationRepository);
infrastructureContainer.registerSingleton(InfrastructureToken.DatasetRepository, DatasetRepository);

export { infrastructureContainer };
