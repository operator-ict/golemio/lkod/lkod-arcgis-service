export const InfrastructureToken = {
    PostgresConnector: Symbol(),
    OrganizationRepository: Symbol(),
    DatasetRepository: Symbol(),
};
