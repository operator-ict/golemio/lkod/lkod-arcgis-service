import { domainContainer } from "#domain/ioc/Di";
import { LkodArcgisServiceApp } from "../LkodArcgisServiceApp";
import { TaskScheduler } from "../TaskScheduler";
import { ApplicationToken } from "./ApplicationToken";

const applicationContainer = domainContainer.createChildContainer();

// Registrations
applicationContainer.registerSingleton(ApplicationToken.TaskScheduler, TaskScheduler);
applicationContainer.registerSingleton(ApplicationToken.LkodArcgisServiceApp, LkodArcgisServiceApp);

export { applicationContainer };
