export interface ITaskScheduler {
    start(): void;
    stop(): void;
}
