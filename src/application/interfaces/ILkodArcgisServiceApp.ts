import { Disposable } from "@golemio/core/dist/shared/tsyringe";

export interface ILkodArcgisServiceApp extends Disposable {
    start(): Promise<void>;
}
