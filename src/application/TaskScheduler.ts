import { DomainToken } from "#domain/ioc/DomainToken";
import { ICronTask } from "#domain/tasks/interfaces/ICronTask";
import { IConfiguration } from "#framework/interfaces/IConfiguration";
import { FrameworkToken } from "#framework/ioc/FrameworkToken";
import { inject, injectable, injectAll } from "@golemio/core/dist/shared/tsyringe";
import { ITaskScheduler } from "./interfaces/ITaskScheduler";

@injectable()
export class TaskScheduler implements ITaskScheduler {
    constructor(
        @inject(FrameworkToken.Configuration) private config: IConfiguration,
        @injectAll(DomainToken.CronTask) private cronTasks: ICronTask[]
    ) {}

    public start(): void {
        for (const cronTask of this.cronTasks) {
            cronTask.register(this.config.defaultTimezone);
        }
    }

    public stop(): void {
        for (const cronTask of this.cronTasks) {
            if (cronTask.isRunning()) {
                cronTask.stop();
            }
        }
    }
}
