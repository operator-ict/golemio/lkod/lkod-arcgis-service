import { DomainToken } from "#domain/ioc/DomainToken";
import { MapperLoader } from "#domain/mappers/MapperLoader";
import { IPostgresConnector } from "#infrastructure/interfaces/IPostgresConnector";
import { InfrastructureToken } from "#infrastructure/ioc/InfrastructureToken";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { ILkodArcgisServiceApp } from "./interfaces/ILkodArcgisServiceApp";
import { ITaskScheduler } from "./interfaces/ITaskScheduler";
import { ApplicationToken } from "./ioc/ApplicationToken";

@injectable()
export class LkodArcgisServiceApp implements ILkodArcgisServiceApp {
    private isBeingDisposed = false;

    constructor(
        @inject(InfrastructureToken.PostgresConnector) private postgresConnector: IPostgresConnector,
        @inject(ApplicationToken.TaskScheduler) private taskScheduler: ITaskScheduler,
        @inject(DomainToken.MapperLoader) private mapperLoader: MapperLoader
    ) {}

    public async start() {
        await this.postgresConnector.connect();
        await this.mapperLoader.initializeMappers();
        this.taskScheduler.start();
    }

    public async dispose() {
        if (this.isBeingDisposed) {
            return;
        }

        this.isBeingDisposed = true;
        this.taskScheduler.stop();
        await this.postgresConnector.disconnect();
    }
}
