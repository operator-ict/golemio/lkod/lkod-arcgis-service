# Golemio LKOD ArcGIS Service

Component of the [Golemio LKOD](https://gitlab.com/operator-ict/golemio/lkod/lkod-general).

Service for fetching ArcGIS datasets from the DCAT-AP 2.0.1 feed and mapping them to the OFN format.

Developed by http://operatorict.cz

![you are here](./docs/assets/lkod_achitecture-arcgis.png)


## Local Installation

### Prerequisites

- Node.js v18 (https://nodejs.org)
- PostgreSQL v15 (https://www.postgresql.org/)
  - extensions: unaccent, pgcrypto

### Installation

Install all dependencies using command:

```
npm install
```

in the application's root directory.

### Build & Run

#### Production

To compile TypeScript code into js (production build):

```bash
npm run build-minimal
# Or npm run build to generate source map files
```

To run the app:

```
npm run start
```

#### Dev/debug

Run via TypeScript (in this case it is not needed to build separately, application will watch for changes and restart on save):

```
npm run dev-start
```

or run with a debugger:

```
npm run dev-start-debug
```

Running the application in any way will load all config variables from environment variables or the .env file. To run, set all environment variables from the `.env.template` file, or copy the `.env.template` file into new `.env` file in root directory and set variables there.

Project uses `dotenv` package: https://www.npmjs.com/package/dotenv
