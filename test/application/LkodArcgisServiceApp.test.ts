import { LkodArcgisServiceApp } from "#application/LkodArcgisServiceApp";
import { ApplicationToken } from "#application/ioc/ApplicationToken";
import { DomainToken } from "#domain/ioc/DomainToken";
import { InfrastructureToken } from "#infrastructure/ioc/InfrastructureToken";
import { DependencyContainer, container as rootContainer } from "@golemio/core/dist/shared/tsyringe";

describe("Application - LkodArcgisServiceApp", () => {
    let oldEnv: NodeJS.ProcessEnv;
    let container: DependencyContainer;

    const connector = {
        connect: jest.fn(),
        disconnect: jest.fn(),
    };

    const scheduler = {
        start: jest.fn(),
        stop: jest.fn(),
    };

    beforeAll(() => {
        oldEnv = process.env;
        process.env = {
            ...oldEnv,
        };
    });

    beforeEach(() => {
        container = rootContainer
            .createChildContainer()
            .registerSingleton(
                InfrastructureToken.PostgresConnector,
                class DummyConnector {
                    public connect = connector.connect;
                    public disconnect = connector.disconnect;
                }
            )
            .registerSingleton(
                ApplicationToken.TaskScheduler,
                class DummyScheduler {
                    public start = scheduler.start;
                    public stop = scheduler.stop;
                }
            )
            .registerSingleton(ApplicationToken.LkodArcgisServiceApp, LkodArcgisServiceApp)
            .registerSingleton(
                DomainToken.MapperLoader,
                class DummyMapper {
                    public initializeMappers = jest.fn();
                }
            );
    });

    afterEach(() => {
        try {
            container.clearInstances();
        } catch {}
        jest.clearAllMocks();
    });

    afterAll(() => {
        process.env = oldEnv;
    });

    // =============================================================================
    // constructor
    // =============================================================================
    describe("constructor", () => {
        test("should create instance", () => {
            const app = container.resolve<LkodArcgisServiceApp>(ApplicationToken.LkodArcgisServiceApp);
            expect(app).toBeInstanceOf(LkodArcgisServiceApp);
        });
    });

    // =============================================================================
    // start
    // =============================================================================
    describe("start", () => {
        test("should connect to database and start scheduler", async () => {
            const app = container.resolve<LkodArcgisServiceApp>(ApplicationToken.LkodArcgisServiceApp);
            await app.start();
            expect(connector.connect).toHaveBeenCalledTimes(1);
            expect(scheduler.start).toHaveBeenCalledTimes(1);
        });
    });

    // =============================================================================
    // dispose
    // =============================================================================
    describe("dispose", () => {
        test("should disconnect from database and stop scheduler", async () => {
            container.resolve<LkodArcgisServiceApp>(ApplicationToken.LkodArcgisServiceApp);
            await container.dispose();
            expect(connector.disconnect).toHaveBeenCalledTimes(1);
            expect(scheduler.stop).toHaveBeenCalledTimes(1);
        });

        test("should not disconnect from database and stop scheduler if already being disposed", async () => {
            const app = container.resolve<LkodArcgisServiceApp>(ApplicationToken.LkodArcgisServiceApp);
            await container.dispose();
            await container.dispose();
            await app.dispose();
            expect(connector.disconnect).toHaveBeenCalledTimes(1);
            expect(scheduler.stop).toHaveBeenCalledTimes(1);
        });
    });
});
