import { TaskScheduler } from "#application/TaskScheduler";
import { ApplicationToken } from "#application/ioc/ApplicationToken";
import { DomainToken } from "#domain/ioc/DomainToken";
import { Configuration } from "#framework/Configuration";
import { FrameworkToken } from "#framework/ioc/FrameworkToken";
import { DependencyContainer, container as rootContainer } from "@golemio/core/dist/shared/tsyringe";

describe("Application - TaskScheduler", () => {
    let oldEnv: NodeJS.ProcessEnv;
    let container: DependencyContainer;

    const cronTasks = [
        {
            register: jest.fn(),
            isRunning: jest.fn().mockReturnValue(false),
            stop: jest.fn(),
        },
        {
            register: jest.fn(),
            isRunning: jest.fn().mockReturnValue(true),
            stop: jest.fn(),
        },
    ];

    beforeAll(() => {
        oldEnv = process.env;
        process.env = {
            ...oldEnv,
            DEFAULT_TIMEZONE: "America/New_York",
        };
    });

    beforeEach(() => {
        container = rootContainer
            .createChildContainer()
            .registerSingleton(FrameworkToken.Configuration, Configuration)
            .registerSingleton(
                DomainToken.CronTask,
                class DummyCronTask1 {
                    public register = cronTasks[0].register;
                    public isRunning = cronTasks[0].isRunning;
                    public stop = cronTasks[0].stop;
                }
            )
            .registerSingleton(
                DomainToken.CronTask,
                class DummyCronTask2 {
                    public register = cronTasks[1].register;
                    public isRunning = cronTasks[1].isRunning;
                    public stop = cronTasks[1].stop;
                }
            )
            .registerSingleton(ApplicationToken.TaskScheduler, TaskScheduler);
    });

    afterEach(() => {
        container.clearInstances();
        jest.clearAllMocks();
    });

    afterAll(() => {
        process.env = oldEnv;
    });

    // =============================================================================
    // constructor
    // =============================================================================
    describe("constructor", () => {
        test("should create instance", () => {
            const scheduler = container.resolve<TaskScheduler>(ApplicationToken.TaskScheduler);
            expect(scheduler).toBeInstanceOf(TaskScheduler);
        });
    });

    // =============================================================================
    // start
    // =============================================================================
    describe("start", () => {
        test("should start all tasks", () => {
            const scheduler = container.resolve<TaskScheduler>(ApplicationToken.TaskScheduler);
            scheduler.start();
            expect(cronTasks[0].register).toHaveBeenNthCalledWith(1, "America/New_York");
            expect(cronTasks[1].register).toHaveBeenNthCalledWith(1, "America/New_York");
        });
    });

    // =============================================================================
    // stop
    // =============================================================================
    describe("stop", () => {
        test("should stop all tasks", () => {
            const scheduler = container.resolve<TaskScheduler>(ApplicationToken.TaskScheduler);
            scheduler.stop();
            expect(cronTasks[0].stop).toHaveBeenCalledTimes(0);
            expect(cronTasks[1].stop).toHaveBeenCalledTimes(1);
        });
    });
});
