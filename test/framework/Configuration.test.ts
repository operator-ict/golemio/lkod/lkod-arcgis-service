import { Configuration } from "#framework/Configuration";
import { IConfiguration } from "#framework/interfaces/IConfiguration";
import { FrameworkToken } from "#framework/ioc/FrameworkToken";
import { DependencyContainer, container as rootContainer } from "@golemio/core/dist/shared/tsyringe";

jest.mock("@golemio/core/dist/helpers/logger", () => {
    return { createLogger: jest.fn().mockReturnValue({ mockLogger: "TEST" }) };
});

describe("Framework - Configuration", () => {
    let oldEnv: NodeJS.ProcessEnv;
    let container: DependencyContainer;

    beforeAll(() => {
        oldEnv = process.env;
        process.env = { ...oldEnv, LOG_LEVEL: "error" };
    });

    beforeEach(() => {
        container = rootContainer.createChildContainer().register(FrameworkToken.Configuration, Configuration);
    });

    afterEach(() => {
        container.clearInstances();
    });

    afterAll(() => {
        process.env = oldEnv;
    });

    // =============================================================================
    // constructor
    // =============================================================================
    describe("constructor", () => {
        test("should create instance", () => {
            const config = container.resolve<IConfiguration>(FrameworkToken.Configuration);
            expect(config).toBeInstanceOf(Configuration);
            expect(config.nodeEnv).toEqual("test");
            expect(config.logLevel).toEqual("error");
        });
    });
});
