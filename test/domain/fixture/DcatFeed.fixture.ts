/* eslint-disable max-len */
export const DcatFeedFixture = [
    {
        "@type": "dcat:Dataset",
        "@id": "https://testsite-iprpraha.hub.arcgis.com/maps/iprpraha::geologie-wgs84",
        "dct:title": "Geologie WGS84",
        "dct:description":
            "<div style='text-align:Left;'><div><div><p><span>Ložiska nerostných surovin</span><span> - toto je description</span></p></div></div></div>",
        "dcat:contactPoint": {
            "vcard:fn": "robert.spal",
            "vcard:hasEmail": "{{orgContactEmail}}",
            "@id": "https://iprpraha.maps.arcgis.com/sharing/rest/community/users/robert.spal?f=json",
            "@type": "vcard:Kind",
        },
        "dct:publisher": {
            "@type": "foaf:Agent",
            "foaf:name": "Institut plánování a rozvoje hlavního města Prahy",
        },
        "dcat:theme": {
            "@type": "skos:Concept",
            "skos:prefLabel": "Geospatial",
        },
        "dct:accessRights": {
            "@type": "dct:RightsStatement",
            "@label": {
                "@value": "public",
            },
        },
        "dct:identifier": "https://www.arcgis.com/home/item.html?id=d659706896e54e2898362b60f740aacb&sublayer=3",
        "dcat:keyword": ["geologie", "nerostné zdroje", "Brownfields", "Parcely"],
        "dct:provenance": {
            "@type": "dct:ProvenanceStatement",
            "@label": {
                "@value": "toto je credits",
            },
        },
        "dct:issued": "2007-11-14T00:00:00",
        "dct:modified": "2022-03-21T00:00:00.000Z",
        "dct:language": {
            "@id": "lang/CZE",
        },
        "dcat:distribution": [
            {
                "@type": "dcat:Distribution",
                "dcat:accessURL": {
                    "@id": "https://testsite-iprpraha.hub.arcgis.com/maps/iprpraha::geologie-wgs84",
                },
                "dct:format": {
                    "@id": "ftype/HTML",
                },
                "dct:description": "Web Page",
                "dct:title": "ArcGIS Hub Dataset",
            },
            {
                "@type": "dcat:Distribution",
                "dcat:accessURL": {
                    "@id": "https://services5.arcgis.com/SBTXIEUGWbqzUecw/arcgis/rest/services/Geologie_WGS84/FeatureServer/3",
                },
                "dct:format": {
                    "@id": "ftype/JSON",
                },
                "dct:description": "Esri REST",
                "dct:title": "ArcGIS GeoService",
            },
            {
                "@type": "dcat:Distribution",
                "dct:title": "Shapefile (S-JTSK)",
                "dct:format": {
                    "@id": "application/zip",
                },
                "dcat:accessURL": {
                    "@id": "https://testsite-iprpraha.hub.arcgis.com/datasets/iprpraha::geologie-wgs84.zip?outSR=%7B%22latestWkid%22%3A3857%2C%22wkid%22%3A102100%7D",
                },
                "dct:description": "",
            },
            {
                "@type": "dcat:Distribution",
                "dct:title": "DGN (S-JTSK)",
                "dct:format": {
                    "@id": "application/zip",
                },
                "dcat:accessURL": {
                    "@id": "https://opendata.iprpraha.cz/CUR/D3M/BD3/S_JTSK/BD3_Bero00_dgn.zip",
                },
                "dct:description": "",
            },
            {
                "@type": "dcat:Distribution",
                "dct:title": "DWG (S_JTSK)",
                "dct:format": {
                    "@id": "application/zip",
                },
                "dcat:accessURL": {
                    "@id": "https://opendata.iprpraha.cz/CUR/D3M/BD3/S_JTSK/BD3_Bero00_dwg.zip",
                },
                "dct:description": "",
            },
            {
                "@type": "dcat:Distribution",
                "dcat:accessURL": {
                    "@id": "https://testsite-iprpraha.hub.arcgis.com/datasets/iprpraha::geologie-wgs84.csv?outSR=%7B%22latestWkid%22%3A3857%2C%22wkid%22%3A102100%7D",
                },
                "dct:format": {
                    "@id": "ftype/CSV",
                },
                "dct:description": "CSV",
                "dct:title": "CSV",
            },
            {
                "@type": "dcat:Distribution",
                "dcat:accessURL": {
                    "@id": "https://testsite-iprpraha.hub.arcgis.com/datasets/iprpraha::geologie-wgs84.geojson?outSR=%7B%22latestWkid%22%3A3857%2C%22wkid%22%3A102100%7D",
                },
                "dct:format": {
                    "@id": "ftype/GEOJSON",
                },
                "dct:description": "GeoJSON",
                "dct:title": "GeoJSON",
            },
            {
                "@type": "dcat:Distribution",
                "dcat:accessURL": {
                    "@id": "https://testsite-iprpraha.hub.arcgis.com/datasets/iprpraha::geologie-wgs84.zip?outSR=%7B%22latestWkid%22%3A3857%2C%22wkid%22%3A102100%7D",
                },
                "dct:format": {
                    "@id": "ftype/ZIP",
                },
                "dct:description": "Shapefile",
                "dct:title": "ZIP",
            },
            {
                "@type": "dcat:Distribution",
                "dcat:accessURL": {
                    "@id": "https://testsite-iprpraha.hub.arcgis.com/datasets/iprpraha::geologie-wgs84.kml?outSR=%7B%22latestWkid%22%3A3857%2C%22wkid%22%3A102100%7D",
                },
                "dct:format": {
                    "@id": "ftype/KML",
                },
                "dct:description": "KML",
                "dct:title": "KML",
            },
        ],
    },
    {
        "@type": "dcat:Dataset",
        "@id": "https://testsite-iprpraha.hub.arcgis.com/maps/iprpraha::arch-byvaly-pozemkovy-katastr",
        "dct:title": "arch byvaly pozemkovy katastr",
        "dct:description": "{{description}}",
        "dcat:contactPoint": {
            "vcard:fn": "robert.spal",
            "vcard:hasEmail": "{{orgContactEmail}}",
            "@id": "https://iprpraha.maps.arcgis.com/sharing/rest/community/users/robert.spal?f=json",
            "@type": "vcard:Kind",
        },
        "dct:publisher": {
            "@type": "foaf:Agent",
            "foaf:name": "Institut plánování a rozvoje hlavního města Prahy",
        },
        "dcat:theme": {
            "@type": "skos:Concept",
            "skos:prefLabel": "Geospatial",
        },
        "dct:accessRights": {
            "@type": "dct:RightsStatement",
            "@label": {
                "@value": "public",
            },
        },
        "dct:identifier": "https://www.arcgis.com/home/item.html?id=b8cb68818a2f4578bf281820387d7c40",
        "dcat:keyword": ["Bezpečnost"],
        "dct:provenance": {
            "@type": "dct:ProvenanceStatement",
            "@label": {
                "@value": "",
            },
        },
        "dct:issued": "2023-05-15T08:24:54.000Z",
        "dct:modified": "2023-05-15T08:24:54.000Z",
        "dct:language": {
            "@id": "lang/ENG",
        },
        "dcat:distribution": [
            {
                "@type": "dcat:Distribution",
                "dcat:accessURL": {
                    "@id": "https://testsite-iprpraha.hub.arcgis.com/maps/iprpraha::arch-byvaly-pozemkovy-katastr",
                },
                "dct:format": {
                    "@id": "ftype/HTML",
                },
                "dct:description": "Web Page",
                "dct:title": "ArcGIS Hub Dataset",
            },
            {
                "@type": "dcat:Distribution",
                "dcat:accessURL": {
                    "@id": "https://gs-pub.praha.eu/imgs/rest/services/arch/byvaly_pozemkovy_katastr/ImageServer/WMTS?",
                },
                "dct:format": {
                    "@id": "ftype/JSON",
                },
                "dct:description": "Esri REST",
                "dct:title": "ArcGIS GeoService",
            },
        ],
    },
];
/* eslint-enable max-len */
