import { domainContainer } from "#domain/ioc/Di";
import { DomainToken } from "#domain/ioc/DomainToken";
import { MapperLoader } from "#domain/mappers/MapperLoader";
import { IprTransformation } from "#domain/transformations/IprTransformation";
import { IConfiguration } from "#framework/interfaces/IConfiguration";
import { FrameworkToken } from "#framework/ioc/FrameworkToken";
import fs from "fs";
import path from "path";
import { IArcgisFeedOrganizationDao } from "#infrastructure/models/interfaces/IArcgisFeedOrganizationDao";

describe("Domain - IprTRansformation", () => {
    let oldEnv: NodeJS.ProcessEnv;

    beforeAll(async () => {
        oldEnv = process.env;
        process.env = {
            ...oldEnv,
            LKOD_CATALOG_URL: "https://api-lkod.rabin.golemio.cz/",
        };
        const config = domainContainer.resolve<IConfiguration>(FrameworkToken.Configuration);
        config.lkodCatalogUrl = process.env.LKOD_CATALOG_URL ?? null;
        await domainContainer.resolve<MapperLoader>(DomainToken.MapperLoader).initializeMappers();
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    afterAll(() => {
        process.env = oldEnv;
    });

    test("should transform arcgis dcat-ap to dcat-ap-cz format", () => {
        const transformer = domainContainer.resolve<IprTransformation>(DomainToken.IprTransformation);
        const dataset = JSON.parse(fs.readFileSync(path.join(__dirname, "../../data/iprDatasetForTransformation.json"), "utf8"));
        const result = transformer.transform(dataset, "f738778a-c180-4d92-9c6d-622719c448a2", {
            slug: "ipr",
        } as IArcgisFeedOrganizationDao);
        const expected = JSON.parse(fs.readFileSync(path.join(__dirname, "../../data/iprTransformedData.json"), "utf8"));
        expect(result).toEqual(expected);
        expect(expected.distribuce.length).toBe(dataset["dcat:distribution"].length - 1);
    });
});
