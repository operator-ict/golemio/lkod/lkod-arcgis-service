import { AbstractCronTask } from "#domain/tasks/AbstractCronTask";
import { FatalError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";

jest.mock("#domain/ioc/Di", () => {
    return {
        domainContainer: {
            resolve: jest.fn().mockReturnValue({
                warn: jest.fn(),
                error: jest.fn(),
            }),
        },
    };
});

jest.mock("croner", () => {
    return {
        Cron: jest.fn().mockReturnValue({
            isRunning: jest.fn().mockReturnValue(true),
            stop: jest.fn(),
        }),
    };
});

describe("Domain - AbstractCronTask", () => {
    let oldEnv: NodeJS.ProcessEnv;

    const cronTaskMocks = {
        execute: jest.fn(),
    };

    class CronTask extends AbstractCronTask {
        protected get cronExpression(): string {
            return "* * * * *";
        }
        protected execute(): void | Promise<void> {
            return cronTaskMocks.execute();
        }
    }

    beforeAll(() => {
        oldEnv = process.env;
        process.env = {
            ...oldEnv,
        };
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    afterAll(() => {
        process.env = oldEnv;
    });

    // =============================================================================
    // register
    // =============================================================================
    describe("register", () => {
        test("should register cron job", () => {
            const task = new CronTask();
            task.register("America/New_York");
            expect(task.isRunning()).toBeTruthy();
        });
    });

    // =============================================================================
    // stop
    // =============================================================================
    describe("stop", () => {
        test("should stop cron job", () => {
            const task = new CronTask();
            task.register("America/New_York");
            task.stop();
            expect(task["cronJob"]?.stop).toBeCalledTimes(1);
        });
    });

    // =============================================================================
    // executeWithLogging
    // =============================================================================
    describe("executeWithLogging", () => {
        test("should execute task", async () => {
            const task = new CronTask();
            await task["executeWithLogging"]();
            expect(cronTaskMocks.execute).toBeCalledTimes(1);
        });

        test("should log error", async () => {
            const task = new CronTask();
            const error = new Error("test error");
            cronTaskMocks.execute.mockRejectedValueOnce(error);
            await task["executeWithLogging"]();
            expect(task["logger"].error).toBeCalledTimes(1);
        });

        test("should log warning", async () => {
            const task = new CronTask();
            const error = new GeneralError("test error");
            cronTaskMocks.execute.mockRejectedValueOnce(error);
            await task["executeWithLogging"]();
            expect(task["logger"].warn).toBeCalledTimes(1);
        });

        test("should throw fatal error", async () => {
            const task = new CronTask();
            const error = new FatalError("test error");
            cronTaskMocks.execute.mockRejectedValueOnce(error);
            await expect(task["executeWithLogging"]()).rejects.toThrowError(error);
        });
    });
});
