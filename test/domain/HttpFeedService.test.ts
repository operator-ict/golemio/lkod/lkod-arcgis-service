import { HttpFeedService } from "#domain/HttpFeedService";
import { DomainToken } from "#domain/ioc/DomainToken";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { DependencyContainer, container as rootContainer } from "@golemio/core/dist/shared/tsyringe";
import axios, { AxiosResponse } from "axios";
import { DcatFeedFixture } from "./fixture/DcatFeed.fixture";

jest.mock("@golemio/core/dist/helpers/logger", () => {
    return { createLogger: jest.fn().mockReturnValue({ mockLogger: "TEST" }) };
});

describe("Domain - HttpFeedService", () => {
    let oldEnv: NodeJS.ProcessEnv;
    let container: DependencyContainer;

    const feedUrl = "http://testsite-iprpraha.hub.arcgis.com/api/feed/dcat-ap/2.0.1";
    const axiosGetMock = jest.spyOn(axios, "get");

    beforeAll(() => {
        oldEnv = process.env;
        process.env = {
            ...oldEnv,
        };
    });

    beforeEach(() => {
        container = rootContainer.createChildContainer().registerSingleton(DomainToken.HttpFeedService, HttpFeedService);
    });

    afterEach(() => {
        container.clearInstances();
        jest.clearAllMocks();
    });

    afterAll(() => {
        process.env = oldEnv;
    });

    // =============================================================================
    // constructor
    // =============================================================================
    describe("constructor", () => {
        test("should create instance", () => {
            const service = container.resolve<HttpFeedService>(DomainToken.HttpFeedService);
            expect(service).toBeInstanceOf(HttpFeedService);
        });
    });

    // =============================================================================
    // getDcatDatasets
    // =============================================================================
    describe("getDcatDatasets", () => {
        test("should return datasets", async () => {
            axiosGetMock.mockResolvedValueOnce({
                data: {
                    "@type": "dcat:Catalog",
                    "dcat:dataset": DcatFeedFixture,
                },
            } as AxiosResponse);

            const service = container.resolve<HttpFeedService>(DomainToken.HttpFeedService);
            const datasets = await service.getDcatDatasets(feedUrl);
            expect(datasets).toHaveLength(DcatFeedFixture.length);
        });

        test("should return empty array when no datasets", async () => {
            axiosGetMock.mockResolvedValueOnce({
                data: {
                    "@type": "dcat:Catalog",
                },
            } as AxiosResponse);

            const service = container.resolve<HttpFeedService>(DomainToken.HttpFeedService);
            const datasets = await service.getDcatDatasets(feedUrl);
            expect(datasets).toEqual([]);
        });

        test("should return empty array when mismatched type", async () => {
            axiosGetMock.mockResolvedValueOnce({
                data: {
                    "@type": "dcat:Dummy",
                    dataset: DcatFeedFixture,
                },
            } as AxiosResponse);

            const service = container.resolve<HttpFeedService>(DomainToken.HttpFeedService);
            const datasets = await service.getDcatDatasets(feedUrl);
            expect(datasets).toEqual([]);
        });

        test("should throw error when axios throws error", async () => {
            axiosGetMock.mockRejectedValueOnce(new Error("test"));

            const service = container.resolve<HttpFeedService>(DomainToken.HttpFeedService);
            await expect(service.getDcatDatasets(feedUrl)).rejects.toThrowError(GeneralError);
        });
    });
});
