import { InfrastructureToken } from "#infrastructure/ioc/InfrastructureToken";
import { ArcgisFeedOrganizationModel } from "#infrastructure/models/ArcgisFeedOrganizationModel";
import { OrganizationRepository } from "#infrastructure/repositories/OrganizationRepository";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { DependencyContainer, container as rootContainer } from "@golemio/core/dist/shared/tsyringe";

describe("Infrastructure - OrganizationRepository", () => {
    let oldEnv: NodeJS.ProcessEnv;
    let container: DependencyContainer;

    beforeAll(() => {
        oldEnv = process.env;
        process.env = {
            ...oldEnv,
        };
    });

    beforeEach(() => {
        container = rootContainer
            .createChildContainer()
            .registerSingleton(InfrastructureToken.OrganizationRepository, OrganizationRepository);
    });

    afterEach(() => {
        container.clearInstances();
    });

    afterAll(() => {
        process.env = oldEnv;
    });

    // =============================================================================
    // constructor
    // =============================================================================
    describe("constructor", () => {
        test("should create instance", () => {
            const repository = container.resolve<OrganizationRepository>(InfrastructureToken.OrganizationRepository);
            expect(repository).toBeInstanceOf(OrganizationRepository);
        });
    });

    // =============================================================================
    // findAll
    // =============================================================================
    describe("findAll", () => {
        test("should return organizations", async () => {
            const repository = container.resolve<OrganizationRepository>(InfrastructureToken.OrganizationRepository);
            const oldFindAll = ArcgisFeedOrganizationModel.findAll;

            ArcgisFeedOrganizationModel.findAll = jest.fn().mockResolvedValueOnce([
                {
                    id: "1",
                    name: "test",
                    arcGisFeed: "https://testsite-iprpraha.hub.arcgis.com/api/feed/dcat-us/1.1.json",
                } as ArcgisFeedOrganizationModel,
            ]);

            const result = await repository.findAll();
            expect(result).toEqual([
                {
                    id: "1",
                    name: "test",
                    arcGisFeed: "https://testsite-iprpraha.hub.arcgis.com/api/feed/dcat-us/1.1.json",
                },
            ]);

            ArcgisFeedOrganizationModel.findAll = oldFindAll;
        });

        test("should throw error", async () => {
            const repository = container.resolve<OrganizationRepository>(InfrastructureToken.OrganizationRepository);
            await expect(repository.findAll()).rejects.toThrow(GeneralError);
        });
    });
});
