import { Configuration } from "#framework/Configuration";
import { FrameworkToken } from "#framework/ioc/FrameworkToken";
import { PostgresConnector } from "#infrastructure/PostgresConnector";
import { InfrastructureToken } from "#infrastructure/ioc/InfrastructureToken";
import { ArcgisFeedOrganizationModel } from "#infrastructure/models/ArcgisFeedOrganizationModel";
import { DatasetModel, DatasetSchema } from "#infrastructure/models/DatasetModel";
import { FatalError } from "@golemio/core/dist/shared/golemio-errors";
import { DependencyContainer, container as rootContainer } from "@golemio/core/dist/shared/tsyringe";

jest.mock("@golemio/core/dist/helpers/logger", () => {
    return { createLogger: jest.fn().mockReturnValue({ mockLogger: "TEST" }) };
});

jest.mock("@golemio/core/dist/shared/sequelize", () => {
    return {
        Sequelize: jest.fn().mockImplementation(() => {
            return {
                authenticate: jest.fn(),
                define: jest.fn(),
                close: jest.fn(),
            };
        }),
        Model: jest.fn().mockImplementation(() => {
            return {
                init: jest.fn(),
            };
        }),
        DataTypes: {},
    };
});

describe("Infrastructure - PostgresConnector", () => {
    let oldEnv: NodeJS.ProcessEnv;
    let container: DependencyContainer;

    const logger = { info: jest.fn(), silly: jest.fn() };

    beforeAll(() => {
        oldEnv = process.env;
        process.env = {
            ...oldEnv,
            POSTGRES_CONN: "postgres://postgres:postgres@localhost:5432/postgres",
            POSTGRES_SCHEMA: "test",
            POSTGRES_IDLE_TIMEOUT: "1000",
            POSTGRES_MIN_CONNECTIONS: "1",
            POSTGRES_MAX_CONNECTIONS: "10",
        };
    });

    beforeEach(() => {
        container = rootContainer
            .createChildContainer()
            .registerSingleton(FrameworkToken.Configuration, Configuration)
            .registerSingleton(
                FrameworkToken.Logger,
                class DummyLogger {
                    public info = logger.info;
                    public silly = logger.silly;
                }
            )
            .registerSingleton(InfrastructureToken.PostgresConnector, PostgresConnector);
    });

    afterEach(() => {
        container.clearInstances();
    });

    afterAll(() => {
        process.env = oldEnv;
    });

    // =============================================================================
    // constructor
    // =============================================================================
    describe("constructor", () => {
        test("should create instance", () => {
            const connector = container.resolve<PostgresConnector>(InfrastructureToken.PostgresConnector);
            expect(connector).toBeInstanceOf(PostgresConnector);
            expect(connector["defaultConnectionString"]).toEqual("postgres://postgres:postgres@localhost:5432/postgres");
            expect(connector["poolIdleTimeout"]).toEqual("1000");
            expect(connector["poolMinConnections"]).toEqual("1");
            expect(connector["poolMaxConnections"]).toEqual("10");
        });
    });

    // =============================================================================
    // connect
    // =============================================================================
    describe("connect", () => {
        test("should connect to database", async () => {
            const connector = container.resolve<PostgresConnector>(InfrastructureToken.PostgresConnector);
            const initModelsMock = jest.spyOn(connector, "initModels" as keyof PostgresConnector).mockResolvedValue();
            const sequelize = await connector.connect();
            expect(sequelize).toBeDefined();
            expect(sequelize.authenticate).toBeCalled();
            expect(initModelsMock).toBeCalled();
        });
    });

    // =============================================================================
    // getConnection
    // =============================================================================
    describe("getConnection", () => {
        test("should return connection", async () => {
            const connector = container.resolve<PostgresConnector>(InfrastructureToken.PostgresConnector);
            jest.spyOn(connector, "initModels" as keyof PostgresConnector).mockResolvedValue();
            const sequelize = await connector.connect();
            const connection = connector.getConnection();
            expect(connection).toBe(sequelize);
        });

        test("should throw error if connection is not established", () => {
            const connector = container.resolve<PostgresConnector>(InfrastructureToken.PostgresConnector);
            expect(() => connector.getConnection()).toThrowError(FatalError);
        });
    });

    // =============================================================================
    // isConnected
    // =============================================================================
    describe("isConnected", () => {
        test("should return true if connection is established", async () => {
            const connector = container.resolve<PostgresConnector>(InfrastructureToken.PostgresConnector);
            jest.spyOn(connector, "initModels" as keyof PostgresConnector).mockResolvedValue();
            await connector.connect();
            expect(await connector.isConnected()).toBeTruthy();
        });

        test("should return false if connection is not established", async () => {
            const connector = container.resolve<PostgresConnector>(InfrastructureToken.PostgresConnector);
            expect(await connector.isConnected()).toBeFalsy();
        });
    });

    // =============================================================================
    // disconnect
    // =============================================================================
    describe("disconnect", () => {
        test("should disconnect from database", async () => {
            const connector = container.resolve<PostgresConnector>(InfrastructureToken.PostgresConnector);
            jest.spyOn(connector, "initModels" as keyof PostgresConnector).mockResolvedValue();
            await connector.connect();
            await connector.disconnect();
            expect(connector["connection"]?.close).toBeCalled();
        });
    });

    // =============================================================================
    // initModels
    // =============================================================================
    describe("initModels", () => {
        test("should initialize models", async () => {
            const connector = container.resolve<PostgresConnector>(InfrastructureToken.PostgresConnector);
            const oldInit = ArcgisFeedOrganizationModel.init;
            ArcgisFeedOrganizationModel.init = jest.fn().mockReturnValue(ArcgisFeedOrganizationModel);
            DatasetModel.init = jest.fn().mockReturnValue(DatasetModel);
            const connection = class DummyConnection {};
            connector["initModels"](connection as any);
            expect(ArcgisFeedOrganizationModel.init).toBeCalledWith(ArcgisFeedOrganizationModel.attributeModel, {
                schema: "test",
                sequelize: connection,
                tableName: ArcgisFeedOrganizationModel.tableName,
            });
            expect(DatasetModel.init).toBeCalledWith(DatasetSchema, {
                schema: "test",
                sequelize: connection,
                tableName: "dataset",
            });
            ArcgisFeedOrganizationModel.init = oldInit;
        });
    });
});
